#!/usr/bin/env ruby
# -*- coding: utf-8 -*-
require 'optparse'
require 'optparse/time'
require 'ostruct'
require 'time'

TIME_STAMP = "%Y/%m/%d (%a) %H:%M:%S"

REL_DHMS = /^([0-9]+):([0-9]+):([0-9]+):([0-9]+)$/
REL_HMS = /^([0-9]+):([0-9]+):([0-9]+)$/
REL_MS = /^([0-9]+):([0-9]+)$/
REL_S = /^([0-9]+)$/

$confirm = false

def parseDurationTimeString(string)
  # return durationList [D, h, m, s]
  if matched = REL_DHMS.match(string)
    return [ matched[1], matched[2], matched[3], matched[4] ].map {|s| s.to_i}
  elsif matched = REL_HMS.match(string)
    return [ 0,  matched[1], matched[2], matched[3] ].map {|s| s.to_i}
  elsif matched = REL_MS.match(string)
    return [ 0, 0, matched[1], matched[2] ].map {|s| s.to_i}
  elsif matched = REL_S.match(string)
    return [ 0, 0, 0, matched[1]].map {|s| s.to_i}
  else
    return nil
  end
end

def carryDurationList(list)
  # return value is carried duration list
  # Carry [D, h, m, s] => [0, 27, 0, 72] => [1, 3, 1, 12]
  if list[3] >= 60
    list[2] = list[2] + list[3] / 60
    list[3] = list[3] % 60
  end
  if list[2] >= 60
    list[1] = list[1] + list[2] / 60
    list[2] = list[2] % 60
  end
  if list[1] >= 24
    list[0] = list[0] + list[1] / 24
    list[1] = list[1] % 24
  end

  return list
end

def getUntilUnixFromDurationList(list)
  # Convert durationList to only sec.
  # Add sec to current UNIX time.
  # Get absolute untilTimestamp.
  sec = list[0] * 86400 + list[1] * 3600 + list[2] * 60 + list[3]
  nowUnix = Time.now.to_i
  untilUnix = nowUnix + sec
  return untilUnix
end

def parseSpecificTimeString(string)
  # Parse user input to UNIX time, and convert to (D, h, m, s)
  # If it's past, convert to the same time in tomorrow.
  # Return values: 1: durationList, 2: untilUnix
  specific     = Time.parse(string)
  specificUnix = specific.to_i
  now          = Time.now
  nowUnix      = now.to_i
  if (specificUnix - nowUnix) < 0
    $confirm = true
    tomorrowUnix = nowUnix + (60 * 60 * 24)
    tomorrow     = Time.at(tomorrowUnix)
    tomorrowDate = tomorrow.strftime("%Y-%m-%d")
    specificTime = specific.strftime("%H:%M:%S")
    # Replace specific time with tomorrow one
    specific     = Time.parse(tomorrowDate + " " + specificTime)
    specificUnix = specific.to_i
  end
  sec = specificUnix - nowUnix
  durationList = carryDurationList([0, 0, 0, sec]) #還需要return嗎？！
  return durationList, specificUnix
end

class Countdown
  def initialize(durationList, untilUnix, execute=nil, title="")
    @durationList = durationList
    @untilTimestamp = Time.at(untilUnix).strftime(TIME_STAMP)
    @untilUnix = untilUnix
    @nowUnix = Time.now.to_i
    @execute = execute
    @title = title
    
    if $confirm
      p "Hahaha"
    end
    updateScreen()
    updateDurationList()
    detectKeyToQuit()
  end

  def clearScreen
    puts "\e[H\e[2J"
  end

  def updateScreen
    clearScreen
    now = Time.now.strftime(TIME_STAMP)
    puts "Now:   #{now}
Until: #{@untilTimestamp}
#{'='*40}
Days:    #{@durationList[0]}
Hours:   #{@durationList[1]}
Minutes: #{@durationList[2]}
Seconds: #{@durationList[3]}
#{'='*40}"    
    puts @title if @title.length > 1
  end

  def detectKeyToQuit
    # 好像完全沒用啊
    begin
      # See if a 'Q' has been typed yet
      while c = STDIN.read_nonblock(1)
        puts "I found a #{c}"
        return true if c == 'Q'
      end
      # No 'Q' found
      false
    rescue Errno::EINTR
      puts "Your device seems a little slow."
      false
    rescue Errno::EAGAIN
      # nothing was ready to be read
      puts "Nothing to be read..."
      false
    rescue EOFError
      # quit on the end of the input stream
      # (user hit CTRL-D)
      puts "CTRL-D catched."
      true
    end
  end
  
  def updateDurationList
    while true do
      nowUnix = Time.now.to_i
      if nowUnix == @nowUnix
        sleep 0.2
      else
        @nowUnix = nowUnix
        @durationList = carryDurationList([0, 0, 0, @untilUnix - @nowUnix])
        updateScreen
        if @durationList.all? {|x| x == 0}
          timesUp
          break
        else
          sleep 0.2
        end
        
      end
    end
  end

  def timesUp
    clearScreen
    system(@execute) if @execute
    puts "Time's up!"
  end

  def interruptByUser
    clearScreen
    puts "Interrupt by user."
  end
  
end


$options = {}
def parseArgv(arguments)
  $options[:specific] = ""
  $options[:duration] = ""
  $options[:execute] = nil
  $options[:title] = ""

  $options_parser = OptionParser.new {|opts|
    opts.banner = "Usage: countdown <duration|-s specific> [-q] [-t string] [-e command]"
    opts.separator "Examples:"
    opts.separator "  countdown 30        # delay 30 seconds"
    opts.separator "  countdown 1:20:30   # delay 1 hour 20 minutes and 30 seconds"
    opts.separator "  countdown -s 23:30  # delay until 11:30 PM"
    opts.separator "Options:"
    # Options

    opts.on("-s", "--specific [DATE TIME]",
            "Set time as a specific one, instead of duration."){|specific|
      $options[:specific] = specific
    }

    opts.on("-e", "--execute [COMMAND]",
            "Execute command on timeup. The command will not be executed on cancel"){|cmd|
      $options[:execute] = cmd
    }

    opts.on("-t", "--title [STRING]",
            "Show string as this countdown's title"){|title|
      $options[:title] = title
    }

    opts.on("-h", "--help",
            "Display this help message."){|none|
      puts opts
    }
  }
  
  $options_parser.parse!(arguments)
end

def initCountdown
  if $options[:specific] != ""
    durationList, untilUnix = parseSpecificTimeString($options[:specific])
  elsif $options[:duration]
    durationList = carryDurationList(parseDurationTimeString($options[:duration]))
    untilUnix = getUntilUnixFromDurationList(durationList)
  end
  Countdown.new(durationList, untilUnix, $options[:execute], $options[:title])
end


def main
  if ARGV.length() == 0 then
    parseArgv(["-h"])
    # p $options
  else
    parseArgv(ARGV)
    # p $options
    if ARGV.length > 0  
      if $options[:specific] != "" #if not empty
        $options[:specific] = $options[:specific].concat(ARGV.join(" "))
      elsif $options[:specific] == ""
        $options[:duration] = ARGV.join("")
      end
    end
    initCountdown
  end
end

# Avoid C-c interruption
Signal.trap(:INT){
  return nil
}

main()

#puts "\e[H\e[2J"
#system 'clear'
#$stdout.flush


