# countdown.rb
Reimplemented [countdown.sh](https://github.com/lzh9102/bash-countdown) in Ruby.

## Requirement
- Ruby

## Installation
You can place `countdown.rb` under any `PATH`. Then add an alias in your `~/.bashrc` or `~/.zshrc` like this

```sh
alias alarm='countdown.rb -e "mplayer -volume 100 ${HOME}/Musiz/National_Anthem_of_USSR.flv" -t "Wake Up!"'
```

After this, you can use `alarm -s 07:00` to wake yourself up in the morning, or `alarm 5:00` to remind you the instant noodle.

## Usage
```
Usage: countdown <duration|-s specific> [-q] [-t string] [-e command]
Examples:
  countdown 30        # delay 30 seconds
  countdown 1:20:30   # delay 1 hour 20 minutes and 30 seconds
  countdown -s 23:30  # delay until 11:30 PM
  countdown -e 'mplayer -volume 100 "${HOME}/Music/National Anthem of USSR.flv"' -t "Wake Up!" -s 09:00
Options:
  -s, --specific [DATE TIME]       Set time as a specific one, instead of duration.
  -e, --execute [COMMAND]          Execute command on timeup. The command will not be executed on cancel
  -t, --title [STRING]             Show string as this countdown's title
  -h, --help                       Display this help message.
```
## Todo List
- Press `q` to abort.
- Prevent `Ctrl-c`
- Check mistakes in time string.

## License
WTFPL 2.0
